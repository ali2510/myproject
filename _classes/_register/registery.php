<?php
namespace framework
{
class _register_registery
{
    //static class can't make object from it
    private function __construct(){}
    private function __clone(){}
    private static  $_instance = array();
    public  static function setinstance($key , $instance)
    {
        if(is_object($instance))
        {
         self::$_instance[$key]=$instance;
         self::$_instance[$key]->in_registry=true;

        }
    } 
    public static function getinstance($key , $default="AHMED")
    {
        if(isset( self::$_instance[$key])){
            return self::$_instance[$key];

        }
        return $default;
    }  
     public static function remove($key)
     {
         //remove instances
         self::$_instance[$key]->in_registry=false;

         unset(self::$_instance[$key]);
     }

}
















class car 
{
    public $color;
    public $producer;
    public $in_registry;
}
class ford 
{
 
        //private static $_instance;  
        /*public static function getinstance()
        {
           if(isset(self::$_instance))
           {
               return self::$_instance;
           }
           return  self::$_instance = new ford();

        } */
    public $owner ="jack";
    public $num_of_employee="5000";
    public function check($car)
    {
      if($car->producer == $this)
      {
        return true;
      }
      return false;
    }
}

registry::setinstance("ford" ,new ford());
$ob = registry::getinstance("ford");
echo "you are in regist " . $ob->in_registry . "</br>"; 
$car = new car();
$car->color="red";
$car->producer=registry::getinstance("ford");
echo registry::getinstance("ford")->check($car);
print_r(registry::getinstance("ford"));
registry::remove("ford");
echo " no you are not in regist " . $ob->in_registry . "</br>"; 

//print_r(registry::getinstance("ford"));

}