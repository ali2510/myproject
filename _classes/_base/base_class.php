<?php
namespace framework
{
    use framework\_metadata_inespector as inspector ;
    use framework\_datatype_stringmethods as StringMethods ;

    class _base_Base
    {
        private $_inspector;
        public function __construct ($array =array())
        {
              $this->_inspector= new inspector($this);
              if(is_array($array) || is_object($array))
              {
                  foreach($array as $key=>$value)
                  {
                      $key =ucfirst($key);
                      $method="set{$key}";
                      $this->$method($value);
                  }
              }
        }
        public function __call($method , $arrguments)
        {
            //chech inespector if empty 
            if(empty($this->_inspector))
            {
                throw new \Exception ("you didn't calling parent __construct "); 
            }
            //check if method set or get
            $getmatch=StringMethods::match($method , "^get([a-zA-Z0-9]+)$");
            if(sizeof($getmatch)>0)
        { 
            /*code get*/
             //split  get
          $propertyname=StringMethods::split($method , "get");
           //echo $propertyname = $propertyname[0];
           $propertyname =lcfirst($propertyname[0]);
           //echo $propertyname = "_{$propertyname}";
           if(property_exists($this ,$propertyname)) 
           {
              
               $meta = $this->_inspector->getPropertyMtea($propertyname);
               //print_r($meta);
               if(isset($meta["@read"])&&isset($meta["@readwrite"]))
               {
                  
                throw new \Exception("write only property"); 

               }
               if($this->$propertyname){
                echo $this->$propertyname;}
               else{return null;}
            }
            
        } //end of first if
            

        $getmatch=StringMethods::match($method , "^set([a-zA-Z0-9]+)$");
        if(sizeof($getmatch)>0)
    { 
        /*code set*/
         //split  set
         $propertyname=StringMethods::split($method , "set");
           //echo $propertyname = $propertyname[0];
           $propertyname =lcfirst($propertyname[0]);
            $propertyname = "_{$propertyname}";
           if(property_exists($this ,$propertyname)) 
           {
              
               $meta = $this->_inspector->getPropertyMtea($propertyname);
               //print_r($meta);
               if(isset($meta["@read"])&&isset($meta["@readwrite"]))
               {
                  
                throw new \Exception("read only property"); 

               }
                $this->$propertyname = $arrguments[0];
                echo $this->$propertyname;
            }
         
    }//end of second if
   

            //match string method class set get
            //split set or get
            //convert to lower case
            //_color
            //check pro is exists 
            //get meta data
            //read write readwrite
        }//end of call function 
        public function __set($propertyname , $value)
        {
           $function ="set".ucfirst($propertyname);
           return $this->$function($value);
           
        }
        public function __get($propertyname)
        {
            $function ="get".ucfirst($propertyname);
            return $this->$function(); 
        }


    }
}
