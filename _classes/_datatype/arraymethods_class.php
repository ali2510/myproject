<?php 
namespace framework
{


    class _datatype_arraymethods
    {

        private function __construct()
        {
   
        }

        private function __clone()
        {

        }
//function we give it any array it clean the empty elements from this array
        public static function clean($array)
        {
           $newarray = array_filter($array, function($item)
            {
               if(empty($item))
               {
                   return true;
               }
                   return false;
               

            });//end of array filter
            return $newarray;
        }//end of clean function

//giv this function array include elements with wide spaces and it will retuen them without any spaces
public static function trim($array)
{
$newarray=array_map(function($item)
{
    return trim($item);
}
,$array);//end of array_map
return $newarray;
}//end of trim function

   public static  function arrayconverter($array ,$return=array() )
   {
    //i need metod to convert multi dimentional array to one dimention array
    foreach($array as $element)
    {
        if(is_array($element) )
        {
         $retuen=self::arrayconverter($element , $return);
        }
        else
        {
            $return[] =$element;
        }
    }
    return $return;
   } //end of convert function
    }
} 