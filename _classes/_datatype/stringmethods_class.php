<?php

//regular expression
// ? => optional
//(red | green | blue)
// list from chracter [abc123/\_]
// [a-zA-Z0-9] list from a to z and from A TO Z and from 0 to 9
// .  match any character
// + repeate any thing before + at least one time EX: great + grandfather => great great .....grand father
// .* get any chracter repeated any number of times 
// ^ if we need any phrase which start with charcater or word we know it
// word $ that meaning get the phrase which end with this word

namespace framework
{
 
    class _datatype_stringmethods
    {
       
        private static $_delimiter = "#" ;

       private function __construct()
        {
              
        }

        private function __clone()
        {

        }

        public static function match( $string , $pattern )
        {
            $pattern = self::normalize($pattern);
         preg_match_all($pattern , $string , $match , PREG_PATTERN_ORDER);
         return $match;
        } 

        private static function normalize($pattern){
            return self::$_delimiter.trim($pattern).self::$_delimiter;
        }

        public static function set_delimiter($value)
        {
            self::$_delimiter =$value;
        }

        public static function get_delimiter()
        {
            return self::$_delimiter;
        }

        public static function split ($string , $pattern ,$limit=-1 )
        {$pattern = self::normalize($pattern);
            $flag = PREG_SPLIT_NO_EMPTY ;
            $match = preg_split($pattern , $string , $limit, $flag) ;
            return $match ;
        }
    }
}