<?php
//any class you work on it put it into namespace framework to know it
namespace framework{
class autoloader
{
   private function __construct()
   {
           //prevent instances
   }

   private function __clone()
   {
        //prevent cloning
   }

/*=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>*/


    //this function must be bublic static
    public static  function autoload($classname)
    {
           //call classname_processing();
          $path =  self::classname_processing($classname);
    //call checkexistance function
    $sign = self::checkexistance($path);
    //if true  :)=> so the file exist and you can make include for it
    if($sign == true){
           
            include $path;
    }
    //if false :(=> theow exception
    }

/*=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<*/


/*=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>*/

    private static function classname_processing($classname)
    {
//convert classname to lower name
$classname = strtolower($classname);
    //_design_car................car
    //array classnamearray explod(_,classname)
     $classnamearray = explode('_',$classname);
    //print_r($classnamearray);
    //count elements in classnamearray
    $counter = count($classnamearray);
    //if count ==1 :)=>=>=>=> this meanning that the file direct in _classes
    if ($counter > 1)
    {
          $filename=$classnamearray[$counter-1];
          $classnamearray[$counter-1] = null; 
          $classnamearray[0] = null;         
          $newclassarray= array();
          foreach($classnamearray as $elem)
          {
                  if(!empty($elem))
                  {
                       $newclassarray[]=$elem;
                  }
          }
           //using function implod ("\_",array)
          $classpath=implode('\_' , $newclassarray);
              //i get path
          return $path ='C:\xampp\htdocs\framework\_classes\_'.$classpath.'\\'.$filename.'_class.php ';

            }
    else
    {
            //if classname => car
         return $path ='C:\xampp\htdocs\framework\_classes\\'.$classname.'.php';
    }
   

    }

/*=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<*/


/*=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>*/

    private static function checkexistance($path)
    {
    //checking existance with function file_exist("path") true
       if(file_exists($path)){
               return true;
       }
    }

}
}
