<?php


namespace framework
{
use framework\_base_Base as Base ;
class _routing_Route extends Base
{
    //share code & control on all routes 
    /**
     * @readwrite
     */
    protected $_pattern;
    /**
     * @readwrite
     */
    protected $_controller;
    /**
     * @readwrite
     */
    protected $_action;
    /**
     * @readwrite
     */
    protected $_parameters =array();
    
}


}
