<?php


namespace framework
{
use framework\_base_Base as Base ;
use framework\_register_registery  as registry ;
use framework\_metadata_inespector as inspector ;


  /**
    * manage routes and redirect to the correct
    *   action and controller   
    */ 
class _routing_router extends Base
{
 /**
  * @readwrite
  */
  protected $_url;
   /**
  * @read
  */
  protected $_action;
  /**
  * @read
  */
  protected $_controller;
  protected $_routes=array();
  //---------------------------------------------
  //functions
  //---------------------------------------------
  public function addroute($route)
  {
    $this->routes[]=$route;
    return $this;
  }
  public function removeroute($route)
  {
      foreach($this->routes as $key=>$value)
      {
        if($route == $value)
        {
          unset($this->routes[$key]);
        }
      }
      return $this;
  }
  public function getallroutes()
  {
      $list =array();
      foreach($this->routes as $route)
      {
        $list[$route->pattern]=get_class($route);
      }
      return $list;
  }
  public function dipatch()
  {
    $url =$this->_url;
    $parameters=array();
    $controller="index";
    $action="index";
    foreach($this->routes as $route )
    {
      $match = $route->match($url);
      if($match)
      {
        echo "jdjdjd";
        $controller =$route->controller;
        $action=$route->action;
        $parameters=$route->parameters;
        $this->_pass($controller ,$action, $parameters);
        return;

      }
      //url = news/addnews/news1/hhh.gpg/12-2-2020
      $parts = explode('/',trim($url,'/'));
      if(sizeof($parts)>0)
      {
        $controller = $parts[0];
      }
      if(sizeof($parts)>1)
      {
        $action = $parts[1];
        $parameters = array_slice($parts , 2); 
      }
      //if it not return any thing we will take controller and action and parameters from url

    }
  }


    protected function _pass($controller ,$action, $parameters)
    {
      //call method -action in class --controller
      //and pass param
      $name = ucfirst($controller);
      $this->_controller=$controller;
       $this->_action=$action;
       try{
       $instance=new $name(array('parameters'=>$parameters));//error
       registry::setinstance('controller',$instance);
       }
       catch(\Exception $exp)
       {
          throw new \Exception("this controller not found");
       }
       if(!method_exists($instance , $action))
       {
         $instance->renderview = false;
         throw new \Exception("this action not found");
       }
       //controller and action are exist
       $inspector = new inspector($instance);
       $metadata = $inspector->getMethodMeta($action);
       if(!empty($metadata['@protected']) | !empty( $metadata['@private']))
       {
        throw new \Exception("not accessable action");

       }
       //@before x y z @after @once
       $hooks= function($meta , $type)use($instance , $inspector)
       {
         if(isset($meta[$type]))
         {
              $run =array();
              foreach($meta[$type] as $method)
              {
                $hookmeta =$inspector->getMethodMeta($method);
                if(!empty($hookmeta['@once']) && in_array($method , $run))//check for once metadata
                {
                   continue;
                }
                   $instance->$method();
                   $run[] =$method;
              }
         }
       };
       $hooks($metadata , '@before');
       call_user_func_array( array($instance , $action),array(is_array($parameters)?$parameters:array())
      
      );
      $hooks($metadata,'@after');
      registry::setinstance('controller');


    } 


}


}
