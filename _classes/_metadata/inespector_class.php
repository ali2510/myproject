<?php
namespace framework
{
    use framework\_datatype_stringmethods as strmethods;

    use framework\_datatype_arraymethods as  arraymethods;

  

    class _metadata_inespector
    {
       /* 
       .................
       PROPERTIES :)
       .................
       property _class => TO STORE CLASS NAME :)
       meta array () class , properties , methods )=> TO STORE META :)
       properties => TO STORE ALL PROPERTIES :)
       methods  => TO STORE ALL METHODS :)
       .................
       FUNCTIONS :)
       .................
       1-protected functions => which takes class name and use reflction to get comments 
       and use it from class and send it to get m eta :)
       ---------------------------------------------------------------------------------------- 
       get_class_comment()
       get_property_comment()
       get_method_comment()
       get_properties()
       get_methods()
       -----------------------------------------------------------------------------------------
       2- this function  will be used by othe functions which user can access it otside the class
       -----------------------------------------------------------------------------------------
       get_meta()
       -----------------------------------------------------------------------------------------
       3- public functions => which user use to get any class_meta or any methods or properities
       and store it into meta
       -----------------------------------------------------------------------------------------
       get_class_meta()
       get_method_meta(method)
       get_properity_meta()
       get_class_methods()
       get_class_properities()  
       */

       // START OF CODE
       /*
       .................
       PROPERITIES :)
       .................
       */
       protected $_class ;
       protected $_meta = array(
                                   "class"         => array() ,
                                   "properties"   => array(),
                                   "methods"       => array(),
       );
       protected $properities;
       protected $methods;

    
    /*
    .................
    FUNCTIONS :)
    .................
    */
    /*if user make object from class inspector he inter the class which he need to get info from it
    and it will use the properities oh this class*/
    public  function __construct($class_name)
    {
        //to get class name
       $this->_class =$class_name;
    }
    /*
    .................
    protected_FUNCTIONS :)
    .................
    */
    protected function _getClassComment()
    {
        //use reflection to get class information
          $obj=new \ReflectionClass($this->_class);
        //return info about class documentation => RETURN COMMENTS BEFORE THE CLASS
        return $obj->getDocComment();

    }
    protected function _getPropertyComment($property)
    {
        $obj=new \ReflectionProperty($this->_class , $property);
        //return info about class documentation => RETURN COMMENTS BEFORE THE PROPRTY
        return $obj->getDocComment(); 
    }
    protected function _getMethodComment($method)
    {
        $obj=new \ReflectionMethod($this->_class , $method);
        //return info about class documentation => RETURN COMMENTS BEFORE THE method
        return $obj->getDocComment(); 
    }
    protected function _getClassMethods()
    {
         //use reflection to get class information
         $obj=new \ReflectionClass($this->_class);
         return $obj->getMethods();
    }
    protected function _getClassProperties()
    {
        //use reflection to get class information
        $obj=new \ReflectionClass($this->_class);
        return $obj->getProperties();
   }
   protected function _getMeta($comment)
   {
       $meta =array();
       $pattern="@[a-zA-Z]+\s*[a-zA-Z0-9,()_]*";
       $match = strmethods::match($comment , $pattern);
       foreach($match as $meta)
       {
          $var =strmethods::split($comment , "[\s]" , 2);
          $var= arraymethods::trim($var);
          $var= arraymethods::clean($var);
          //$meta[$var['1']]=true;
          if(count($var)>1)
          {
            $values =strmethods::split($comment , "[,]" );
            $values= arraymethods::trim($values);
            $values= arraymethods::clean($values);
            $meta[$var[0]]=$values;  
          }
       }
       return $meta;
       }
    //-----------------------------------------------------------------
    /*
    .................
    public_FUNCTIONS :)

    LOOK :)(: THIS FUNCTION WILL NEED getMeta Function
    .................
    */
    public function getClassMeta()
    {
        if(empty($this->_meta["class"]))
        {
            $comment = $this->_getClassComment();
            if(!empty($comment))
            {
               $this->_meta["class"] = $this->_getMeta($comment); 
            }
            else
            {
                $this->_meta["class"]  = null;
            }
        }
        return $this->_meta["class"];
    }//END METHOD GET CLASS META
    //--------------------------------------------------------------------
    public function getClassProperties()
    {
        if(!isset($this->properties))
        {
           $properities=$this->_getClassProperties();
           foreach($properities as $property)
           {
               $this->properties[] = $property;
           }
          
        }  
        return $this->properties;
    }//END OF GET CLASS PROPERTIES
    //---------------------------------------------------------------------
    public function getClassMethods()
    {
        if(!isset($this->methods))
        {
           $methods=$this->_getClassMethods();
           foreach($methods as $method)
           {
               $this->methods[] = $method;
           }
          
        }  
        return $this->methods; 
    }//END OF GET CLASS METHOD
    //---------------------------------------------------------------------
    public function getPropertyMtea($property)
    {
        if(!isset($this->_meta["properties"][$property]))
        {
            $comment = $this->_getPropertyComment($property);
            if(!empty($comment))
            {
               $this->_meta["properties"][$property] = $this->_getMeta($comment); 
            }
            else
            {
                $this->_meta["properties"][$property]  = null;
            }
        }
        return $this->_meta["properties"][$property];
    }//END OF GET PROPERTY META
    //---------------------------------------------------------------------
    public function getMethodMeta($method)
    {
        if(!isset($this->_meta["methods"][$method]))
        {
            $comment = $this->_getMethodComment($method);
            if(!empty($comment))
            {
                $this->_meta["methods"][$method] = $this->_getMeta($comment); 
            }
            else
            {
                $this->_meta["methods"][$method]  = null;
            }
        }
        return $this->_meta["methods"][$method]; 
    }//END OF GET METHODS META
    
 
}
}